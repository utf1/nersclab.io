# NERSC Technical Documentation

docs.nersc.gov is a resource with the technical details for users to
make effective use of [NERSC](https://nersc.gov)'s resources. For
center news and information visit
the [NERSC Home page](https://nersc.gov) and for interactive content
visit [MyNERSC](https://my.nersc.gov).

!!! tip 
	These pages are hosted from a 
	[git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
	[contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/master/CONTRIBUTING.md)
	are welcome!
	
	[Fork this repo](https://gitlab.com/NERSC/nersc.gitlab.io/-/forks/new)

## Quick links

 1. [Example Jobs](jobs/examples/index.md)
 1. [Queue Policies](jobs/policy.md)
 1. [Running Jobs](jobs/index.md)
 1. [Best Practices](jobs/best-practices.md)
 1. [Jupyter](connect/jupyter.md)
 1. [File Permissions](filesystems/unix-file-permissions.md)
 1. [Building Software](programming/compilers/wrappers.md)
 1. [Managing Data](data/management.md)
 1. [Password Reset](accounts/index.md#forgotten-passwords)
