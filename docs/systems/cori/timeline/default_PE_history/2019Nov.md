# Programming Environment Change on Cori in November 2019

## Background

During the scheduled maintenances on Cori (Oct 31 - Nov 1), we will upgrade the OS from CLE7.0UP00 and CLE7.0UP01, and install the new Cray Programming Environment Software release CDT/19.09.  There will be no software default versions change this time.

Below is the detailed list of changes. See [Cray monthly CDT release notes](https://pubs.cray.com/browse/xc/article/released-cray-xc-programming-environments) for full list of software in each CDT.  

## New software versions available

* cce/9.0.2
* cray-hdf5/1.10.5.1
* cray-hdf5-parallel/1.10.5.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.10
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.3.1
* craype/2.6.1
* craype-dl-plugin-py2, craype-dl-plugin-py3/19.09.1
* iobuf/2.0.9
* modules/3.2.11.3
* papi/5.7.0.2
* perftools, perftools-base, perftools-lite/7.1.1


## Important Information Regarding CCE 9.0 Compatibility  (note: default in Cori is still the classic cce/8.7.9)

CCE 9.0 includes changes that require updated versions of many libraries, including MPI, for compatibility with CCE 9.0.  The libraries in the June/19.06 (or later) PE release are compatible with CCE 9.0, but are not compatible with CCE 8.7.   If a CCE user needs to go back to a product from a previous PE release, they should change both CCE and MPI, and any other dependent libraries.  For example, CCE 9.0 would use the 19.06 PE library cray-mpich 7.7.8 or later, while CCE 8.7 would use cray-mpich 7.7.7 or earlier.

Also, the CCE 9.0 C and C++ compiler is now based on Clang/LLVM.  Users need to adjust makefiles to use Clang command line syntax, which is similar to the GNU compiler syntax.  Fortran is not affected.  The previous C and C++ compiler, now referred to as CCE Classic C and C++, is available with CCE 9.0 to support a transition period.  CCE Classic C and C++ will be discontinued in a future release.

It is recommended that CCE users read the CCE 9.0 release notes for more detailed compatibility information. The CCE 9.0 release notes may be viewed using the command "module help cce/9.0.0".

A cdt/19.06 (or later) module is provided to help users load required product versions for compatibility with CCE 9.0.  


## Default linking mode change in CDT/19.09 (note: default on Cori is still CDT/19.03, with static linking)

Beginning with the PE 19.06 release, the default linking mode on XC Cray systems becomes dynamic.  This change is being made in version 2.6.0 of craype, and applies to all versions of all compilers, including CCE.  Static linking will still be a non-default option, where supported.  Applications that have already been built are unaffected by this change.  From the time that the change is made in craype, anything linked will be linked dynamically by default.  Thus, swapping to a craype version prior to June/19.06 will result in static linking by default; while swapping to the June/19.06 version (or later) of craype will result in dynamic linking by default.  The default can be overridden either on the command line, with the '-static' flag, or by setting  'CRAYPE_LINK_TYPE=static' in the environment.  