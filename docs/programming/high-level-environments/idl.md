#IDL

IDL's primary use is in the analysis and display of scientific data through its
programming, plotting, and image display facilities.

##Features

* 2-D Plotting & Contouring
* Surface Plotting & 3-D Graphics
* Graphic Effects
* Color Systems
* Curve & Surface Fitting
* Image and Signal Processing
* Eigensystems
* Linear Systems
* Sparse Linear Systems
* Nonlinear Systems and Root Finding
* Multi-Dimensional Optimization
* Special & Transcendental Functions
* Correlation Analysis and Forecasting
* Hypothesis Testing
* Multi-Dimensional Gridding and Interpolation
* Mapping
* Development and Programming Tools
* Integrated Development Environment
* User Interface Toolkit
* IDL Insight
* IDL DataMiner Option

##Using IDL at NERSC

First load the IDL module

```
module load idl
```

and start IDL by running
```
idl
```
Or to use the IDL development environment, type
```
idlde
```

IDL has a rich set of demos. To view them, in the **IDL command line**, type:
```
demo
```

##Availability at NERSC

We currently support IDL 8.5 (default), 8.3, and 8.2.

##Additional resources

For more information on IDL, please refer to the IDL software home
[page](https://www.harrisgeospatial.com/Software-Technology/IDL).

