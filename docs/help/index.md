# NERSC Help and Support

NERSC strives to be the most user friendly supercomputing center in
the world.

## FAQ

* [Password Resets](../accounts/passwords.md#forgotten-passwords)
* [Connection problems](../connect/ssh.md)
* [File Permissions](../filesystems/unix-file-permissions.md)

## Help Desk

The [online help desk](https://help.nersc.gov/) is the **preferred**
method for contacting NERSC.

!!! attention
	NERSC Consultants handle thousands of support requests per
	year. In order to ensure efficient timely resolution of issues
	include **as much of the following as possible** when making a
	request:

	* error messages
	* jobids
	* location of relevant files
	     * input/output
	     * job scripts
	     * source code
	     * executables
	* output of `module list`
	* any steps you have tried
	* steps to reproduce

### Phone support

The [ticketing system](https://help.nersc.gov/) is the preferred method
of contacting NERSC. NERSC staff can be reached at 1-800-66-NERSC
(USA) or 510-486-8600 (local and international). Consulting and
account support are only available during NERSC business hours (8-5 US
Pacific). Password reset requests are available through NERSC
Operations 24/7.
